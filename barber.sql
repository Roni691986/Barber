-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2017 at 08:45 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barber`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `year` varchar(11) NOT NULL,
  `month` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `hour` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`ID`, `name`, `phone`, `type`, `note`, `year`, `month`, `day`, `hour`) VALUES
(196, '4444444', '4444444444', 'החלקה', '', '2017', '04', '14', '09:00'),
(202, 'ddd', '2222222222', 'קצוץ', '', '2017', '05', '09', '09:00'),
(205, 'dsaf', '2222222222', 'פן', '', '2017', '04', '11', '11:00'),
(206, 'dsaf', '2222222222', 'פן', '', '2017', '04', '11', '11:00'),
(201, 'ddd', '2222222222', 'קצוץ', '', '2017', '05', '10', '09:00'),
(200, 'dd', '2222222222', 'החלקה', '', '2017', '05', '23', '09:00'),
(199, 'dd', '2222222222', 'החלקה', '', '2017', '05', '08', '09:00'),
(198, 'dd', '2222222222', 'החלקה', '', '2017', '04', '02', '09:00'),
(197, 'ff', '4444444444', 'קצוץ', '', '2017', '04', '04', '09:00'),
(195, '4444444', '4444444444', 'החלקה', '', '2017', '04', '13', '09:00'),
(193, 'e', '2222222222', 'קצוץ', '', '2017', '05', '08', '09:00'),
(192, 'e', '2222222222', 'קצוץ', '', '2017', '04', '02', '09:00'),
(191, 'e', '2222222222', 'קצוץ', '', '2017', '04', '28', '09:00'),
(190, 'e', '2222222222', 'קצוץ', '', '2017', '04', '12', '09:00'),
(189, 'dsf', '2222222222', 'פן', '', '2017', '04', '07', '09:00'),
(187, 'f', '3333333333', 'החלקה', '', '2017', '04', '26', '09:00'),
(185, 'd', '3333333333', 'החלקה', '', '2017', '04', '04', '16:30'),
(184, 'f', '3333333333', 'קצוץ', '', '2017', '04', '19', '09:00'),
(182, 'fff', '3333333333', 'קצוץ', '', '2017', '04', '18', '09:00'),
(180, 'רוני', '1111111111', 'החלקה', '', '2017', '04', '02', '16:00');

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `ID` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`ID`, `user`, `password`) VALUES
(1, 'Roni', '827ccb0eea8a706c4c34a16891f84e7b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;
--
-- AUTO_INCREMENT for table `manager`
--
ALTER TABLE `manager`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
