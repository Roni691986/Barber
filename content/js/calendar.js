var date = new Date();
var day = date.getDate();
var month = date.getMonth()+1;
var year = date.getFullYear();
var jumpToDay = 0;
(function() {
	[].slice.call(document.querySelectorAll('select.cs-select'))
	.forEach(function(el) {
		new SelectFx(el);
	});
})();

$(function() {
	init();
	$(".pagerBT").click(function(){
		$(".week").hide();
		$(".weekTable").hide();
		$(".monthTable").hide();
		$(".dayTable").hide();
		$(".pagerBT").removeClass('currentViewTab');
		$(this).addClass('currentViewTab');
		var type = $(this).attr("id");
		//bring data from sql

		switch (type) {
		case 'dayView':
			getDayView();
			$(".dayTable").show();
			break;
		case 'weekView':
			getWeekView();
			$(".week").show();
			break;
		case 'monthView':
			if ($('.fc-today').length == 1){
				$('.fc-today').parent().prevAll('.fc-row').find(".addCustomer").remove();
				$('.fc-today').prevAll('.newDayCustomer').find(".addCustomer").remove();
			}
			$(".monthTable").show();
			break;
		}
	});

});

/***************************************************************************************/
/**********************************CLICKS**********************************************/
/*************************************************************************************/
$(document).on("click", ".btn-danger", function(){
	var id = $(this).attr("data-value");
	$.post( "db.php", { cancelOrder: "cancelOrder" ,id: id})
	.done(function(  ) {
		$("#tr"+id).remove();
	});
});

$(document).on("click", ".addCustomer", function(){
	$( '.group label' ).css("color","#000");
	$( 'div.cs-skin-underline' ).css("background","#ccc");

		
	$("#orderModal").modal();

});



/***************************************************************************************/
/**********************************FUNCTIONS**********************************************/
/*************************************************************************************/
function init(){
	particles();
	$(".weekTable").hide();
	$(".dayTable").hide();

	
}


function getDayView(){
	
	$(".custab").html("");
	if (parseInt(day) < 10) 
		day = "0"+parseInt(day);
	if (parseInt(month) < 10) 
		month = "0"+parseInt(month);



	$.post( "db.php", { getCustomers: "getCustomers" ,day: day ,month: month ,year: year})
	.done(function( data ) {
		var data = $.parseJSON( data );
		var string = "";
		var thead = '<thead><tr><th class="text-right">שם</th><th class="text-right">טלפון</th><th class="text-right">תאריך</th><th class="text-right">שעה</th><th class="text-right">סוג תספורת</th><th class="text-right">הערות</th><th class="text-right">פעולה</th></tr></thead>';
		$.each(data, function(i, item) {
			string += "<tr id=tr"+ item['ID'] +"><td>"+ item['name'] +"</td><td class='phoneLink'><a href='tel:"+item['phone']+"'>"+ item['phone'] +"</a></td>"+
			"<td>"+ item['year'] +" / "+ item['month'] +" / "+ item['day'] +" </td><td style='color:cornflowerblue; font-weight:bold'>"+ item['hour'] +"</td><td>"+ item['type'] +"</td><td>"+ item['note'] +"</td><td class='text-right'>"+
			"<a href='#' class='btn btn-danger btn-xs' data-value="+ item['ID'] +"><span class='glyphicon glyphicon-remove'></span> מחיקה </a></td></tr>";
		}); 
		if (string == "" && $('.noCustomers').length == 0)
			$(".dayTable").append("<p class='noCustomers'>אין רשומות להיום</p>");
		$(".custab").append(thead);
		$(".custab").append(string);


	});
}
function moveToDate(jumpToDay){
	date = new Date(new Date().getTime() + jumpToDay * 24 * 60 * 60 * 1000);
	day = date.getDate();
	month = date.getMonth()+1;
	year = date.getFullYear();
	init();
}

function getWeekView(){
	$(".weekTable").html("");

	var curr = new Date; // get current date
	var day = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
	var last = day + 6; // last day is the first day + 6


	console.log(day);

	$.post( "db.php", { getCustomersWeek: "getCustomersWeek" ,day: day ,month: month ,year: year})
	.done(function( data ) {
		var data = $.parseJSON( data );
		for (var i=day; i<last; i++){
			var str = '';
			var thead = '<table class="table table-striped"><thead><tr><th class="text-center">שעה</th><th class="text-center">שם</th><th class="text-center">טלפון</th><th class="text-center">פעולה</th></tr></thead>';

			$.each(data, function(index, item) {
				if (data[index]['day'] == i){
					str +=("<tr id=tr"+ item['ID'] +"><td style='color:cornflowerblue; font-weight:bold'>"+ item['hour'] +"</td><td>"+ item['name'] +"</td><td><a href='tel:0502560005'>"+ item['phone'] +"</a></td> </td><td class='text-center'>"+
							"<a href='#' class='btn btn-danger btn-xs' data-value="+ item['ID'] +"><span class='glyphicon glyphicon-remove'></span></a></td></tr>");
				}
			}); 
			$(".weekTable").append(thead + str + "</table>");
		}
		

		
		$(".weekTable").show();

	});
}

function particles(){

	$.getScript("content/js/particles.min.js", function(){
		particlesJS('particles-js',
				{
			"particles": {
				"number": {
					"value": 80,
					"density": {
						"enable": true,
						"value_area": 800
					}
				},
				"color": {
					"value": "#ffffff"
				},
				"shape": {
					"type": "circle",
					"stroke": {
						"width": 0,
						"color": "#000000"
					},
					"polygon": {
						"nb_sides": 5
					},
					"image": {
						"width": 100,
						"height": 100
					}
				},
				"opacity": {
					"value": 0.5,
					"random": false,
					"anim": {
						"enable": false,
						"speed": 1,
						"opacity_min": 0.1,
						"sync": false
					}
				},
				"size": {
					"value": 5,
					"random": true,
					"anim": {
						"enable": false,
						"speed": 40,
						"size_min": 0.1,
						"sync": false
					}
				},
				"line_linked": {
					"enable": true,
					"distance": 150,
					"color": "#ffffff",
					"opacity": 0.4,
					"width": 1
				},
				"move": {
					"enable": true,
					"speed": 6,
					"direction": "none",
					"random": false,
					"straight": false,
					"out_mode": "out",
					"attract": {
						"enable": false,
						"rotateX": 600,
						"rotateY": 1200
					}
				}
			},
			"interactivity": {
				"detect_on": "canvas",
				"events": {
					"onhover": {
						"enable": true,
						"mode": "repulse"
					},
					"onclick": {
						"enable": true,
						"mode": "push"
					},
					"resize": true
				},
				"modes": {
					"grab": {
						"distance": 400,
						"line_linked": {
							"opacity": 1
						}
					},
					"bubble": {
						"distance": 400,
						"size": 40,
						"duration": 2,
						"opacity": 8,
						"speed": 3
					},
					"repulse": {
						"distance": 200
					},
					"push": {
						"particles_nb": 4
					},
					"remove": {
						"particles_nb": 2
					}
				}
			},
			"retina_detect": true,
			"config_demo": {
				"hide_card": false,
				"background_color": "#b61924",
				"background_image": "",
				"background_position": "50% 50%",
				"background_repeat": "no-repeat",
				"background_size": "cover"
			}
				}
		);

	});
}

var dayClicked;
var monthClicked;
var yearClicked;
var dateProperties;
var monthSum =0;
$(function() {
	var cal = $( '#calendar' ).calendario( {
		onDayClick : function( $el, $contentEl, dateProperties ) {
			dateProperties = dateProperties;


			for( var key in dateProperties ) {
				if(key == 'day')
					dayClicked = dateProperties[ key ];
				if (key == 'month')
					monthClicked = dateProperties[ key ];
				if (key == 'year')
					yearClicked = dateProperties[ key ];
			}
			
		}
	} ),
	$month = $( '#custom-month' ).html( cal.getMonthName() ),
	$year = $( '#custom-year' ).html( cal.getYear() );

	$( '#custom-next' ).on( 'click', function() {

		setTimeout(function(){
			monthSum++;
			if(monthSum < 0){
				$('.newDayCustomer > .addCustomer').remove();
			}
			if (monthSum == 0){
				$('.fc-today').parent().prevAll('.fc-row').find(".addCustomer").remove();
				$('.fc-today').prevAll('.newDayCustomer').find(".addCustomer").remove();
			}
		});
		
		cal.gotoNextMonth( updateMonthYear );
	} );
	$( '#custom-prev' ).on( 'click', function() {
		monthSum--;

			setTimeout(function(){
				if(monthSum < 0){
					$('.newDayCustomer > .addCustomer').remove();
				}
				if (monthSum == 0){
					$('.fc-today').parent().prevAll('.fc-row').find(".addCustomer").remove();
					$('.fc-today').prevAll('.newDayCustomer').find(".addCustomer").remove();
				}
			});
		cal.gotoPreviousMonth( updateMonthYear );
	} );
	$( '#custom-current' ).on( 'click', function() {
		monthSum = 0;
		setTimeout(function(){
			$('.fc-today').parent().prevAll('.fc-row').find(".addCustomer").remove();
			$('.fc-today').prevAll('.newDayCustomer').find(".addCustomer").remove();
		});
		cal.gotoNow( updateMonthYear );
	} );

	function updateMonthYear() {				
		$month.html( cal.getMonthName() );
		$year.html( cal.getYear() );
	}

	$("#acceptSchedule").click(function(){
		$(".error").css("bottom","3%");
		if ($("#name").val() == ""){
			$(".error").html("אנא מלא שם");
			return false;
		}
		else
			$(".error").html("");

		if ($("#phone").val() == ""){
			$(".error").html("אנא מלא טלפון");
			return false;
		}
		else
			$(".error").html("");
		if ($("#phone").val().length < 9){
			$(".error").html("מספר טלפון אינו תקין");
			return false;
		}
		else
			$(".error").html("");


		if ($("#hairCutOption option:selected").html().indexOf("תספורת") >= 0){
			$(".error").html("אנא בחר סוג תספורת");
			return false;
		}
		else
			$(".error").html("");

		var name = $("#name").val();
		var type = $(".cs-select option:selected").html();



		var name = $("#name").val();
		var phone = $("#phone").val();
		var note = $("#info").val();
		var type = type;
		var hour = $("#selectHour").find("option:selected").text();

		if (dayClicked < 10) 
			dayClicked = "0"+dayClicked;

		if (monthClicked < 10) 
			monthClicked = "0"+monthClicked;

		//send to php
		$.post( "db.php", { newCustomer: "newCustomer", name: name , hour: hour ,day: dayClicked ,month: monthClicked ,year: yearClicked ,type: type ,note: note,phone: phone})
		.done(function( data ) {
			console.log( "Data Loaded into DB "); 
			if (dayClicked < 10) dayClicked = dayClicked.charAt(1);
			if (monthClicked < 10) monthClicked = monthClicked.charAt(1);
			$("[data-id="+dayClicked+"]").append("<article class='event'><span class='eventHour'>"+hour+"</span><span class='eventName'>"+name+"</span><span class='glyphicon glyphicon-remove'></span></article>");
			
		});



	});

	$('input[name="Phone"]').keypress(function() {
		if (this.value.length >= 10) {
			return false;
		}
	});
});












