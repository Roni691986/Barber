var day;
var month;
var year;
var hoverTwice = false;

$(function() {
	$.getScript( "https://maps.googleapis.com/maps/api/js?key=AIzaSyDrMpVl8ipblC_UEBo1HS4jbZkgHhqKPJg&callback=initMap");

	$("header").height(screen.height);
	$("#schedule").height(screen.height);
	$("#schedule>div>.row").height(screen.height - (screen.height/4));
	
	
	
	var hairCut = 1;
	var disabledDates = [];
	var date = new Date();
	var month = date.getMonth()+1;
	var year = date.getFullYear(); 

	$('#datepicker').datepicker({
		regional: "he", 
		minDate: new Date(),
		beforeShowDay: $.datepicker.noWeekends,
		onSelect: function(dateText, inst) { 
			var currentDate = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val().split("/");
			openTimePicker(currentDate);
		}
	});

	$('#loginModal').on('hidden.bs.modal', function (e) {
		var inputs = $('form input');
		var title = $('.modal-title');
		var progressBar = $('.progress-bar');
		var button = $('.modal-footer button');

		inputs.removeAttr("disabled");

		title.text("מסך כניסה");

		progressBar.css({ "width" : "0%" });

		button.removeClass("btn-success")
		.addClass("btn-primary")
		.text("Ok")
		.removeAttr("data-dismiss");

	});
});



/***************************************************************************************/
/**********************************CLICKS**********************************************/
/*************************************************************************************/



$('input[name="Phone"]').keypress(function() {
	if (this.value.length >= 10) {
		return false;
	}
});

$(".navbar-nav li").mouseenter(function () {
	$("audio")[0].play();
});

$(".hour").click(function(){
	$(".hour").removeClass("current");
	$(this).addClass("current");
	if (hoverTwice == true)
		$(this).next().addClass("current");
	

	$('.newOrder').fadeIn('slow');
	$(".submitNewOrder").fadeIn('slow').delay(2000);
});

$("#arrowDownToClendar").click(function(){

	if ($("#name").val() == ""){
		$(".error").html("אנא מלא שם");
		return false;
	}
	else
		$(".error").html("");

	if ($("#phone").val() == ""){
		$(".error").html("אנא מלא טלפון");
		return false;
	}
	else
		$(".error").html("");
	
	if ($("#phone").val().length < 9){
		$(".error").html("מספר טלפון אינו תקין");
		return false;
	}
	else
		$(".error").html("");

	
	$( ".details" ).animate({
		opacity: 0,
		marginTop: "-100vw",
	}, 1000);
	$("#arrowUpToClendar").fadeIn('slow').delay(2000);
	$("#datepicker").fadeIn('slow').delay(200);
	setTimeout(function(){
		$("#arrowDownToClendar").css("display","none");
	},100);

});
$("#arrowUpToClendar").click(function(){
	$( ".details" ).animate({
		opacity: 1,
		marginTop: "0vw",
	}, 1000);
	
	$("#arrowUpToClendar").fadeOut('slow').delay(2000);
	$("#arrowDownToClendar").fadeIn('slow').delay(2000);
	$(".submitNewOrder").css("display","none");
	$("#datepicker").fadeOut('slow').delay(200);
	$(".hoursWrapper").fadeOut('slow').delay(2000);
	$("#arrowUpToClendar").css("display","none");

});


$(".loginButton").click(function(){
	if (!!$.cookie('userLogin')){
		console.log(JSON.parse($.cookie("userLogin")));
		var user = JSON.parse($.cookie("userLogin")).user;
		var password = JSON.parse($.cookie("userLogin")).password;
		$.post( "db.php", { loginForCookie: "loginForCookie", user: user , password: password})
		.done(function( data ) {
			if (data == true){
				setTimeout(function(){ window.location.href = "calendar.php"; });
			}
			else
				alert( "wrong details in login cookie");
			//show message wrong password!
		});
	}
});

$(".loginManager").click(function(){
	var user = $("#uLogin").val();
	var password = $("#uPassword").val();

	//check user login
	$.post( "db.php", { login: "login", user: user , password: password})
	.done(function( data ) {
		if (data == true){
			if($('#rememberMe').is(':checked') == true){
				password = md5(password);
				var userLogin = {'user': user, 'password': password}; 
				$.cookie( 'userLogin', JSON.stringify(userLogin) );
			}
			setTimeout(function(){ window.location.href = "calendar.php"; });
		}
	});

});


$('.modal-footer button').click(function(){
	var button = $(this);

	if ( button.attr("data-dismiss") != "modal" ){
		var inputs = $('form input');
		var title = $('.modal-title');
		var progress = $('.progress');
		var progressBar = $('.progress-bar');
		inputs.attr("disabled", "disabled");
		button.hide();
		progress.show();
		progressBar.animate({width : "100%"}, 100);

		progress.delay(1000)
		.fadeOut(600);

		button.text("אנא נסה שוב")
		.removeClass("btn-primary")
		.addClass("btn-success")
		.blur()
		.delay(1600)
		.fadeIn(function(){
			title.text("מסך כניסה");
			button.attr("data-dismiss", "modal");
		});
	}
});
$("#acceptSchedule").click(function(){
	var name = $("#name").val();
	var phone = $("#phone").val();
	var hour = $('.hour.current').html();
	var type = $(".cs-select span").html();
	var note = $("#info").val();

	//send to php
	$.post( "db.php", { newCustomer: "newCustomer", name: name , hour: hour ,day: day ,month: month ,year: year ,type: type ,note: note,phone: phone})
	.done(function( data ) {
		console.log( "Data Loaded into DB ");
		$(".submitNewOrder span").html("תודה!");
		
		$("#reminderModal").modal();
		$("#reminderModal .modal-body p").html(name +" נא לרשום את התאריך והשעה: " + day + "/" + month + "/" + year + " בשעה: " + hour );
		//TODO: open
		$("#datepicker td").css("pointer-events", "none");
		$("#schedule input").css("pointer-events", "none");
		$(".cs-placeholder").css("pointer-events", "none");
		$(".ui-datepicker-next").css("pointer-events", "none");
		$(".ui-datepicker-prev").css("pointer-events", "none");
		$(".hour").css("pointer-events", "none");
		$(".submitNewOrder").css("pointer-events", "none");
	});

});

$(".submitNewOrder").click(function(){

	if ($("#name").val() == ""){
		$(".error").html("אנא מלא שם");
		return false;
	}
	else
		$(".error").html("");

	if ($("#phone").val() == ""){
		$(".error").html("אנא מלא טלפון");
		return false;
	}
	else
		$(".error").html("");
	if ($("#phone").val().length < 9){
		$(".error").html("מספר טלפון אינו תקין");
		return false;
	}
	else
		$(".error").html("");


	if ($(".cs-select span").html().indexOf("תספורת") >= 0){
		$(".error").html("אנא בחר סוג תספורת");
		return false;
	}
	else
		$(".error").html("");

	if (day == undefined){
		$(".error").html("אנא בחר יום");
		return false;
	}
	if ($(".hour.current").length == 0){
		$(".error").html("אנא בחר שעה");
		return false;
	}
	else
		$(".error").html("");

	$("#info").removeAttr("required");
	var name = $("#name").val();
	var hour = $(".hour.current").html();
	var type = $(".cs-select span").html();
	$("#myModal .modal-body p").html("תודה " + "<span class='modal-details'>" + name + "</span>" + ", האם אתה בטוח שברצונך להזמין תור לשעה " + "<span class='modal-details'>" + hour + "</span><br>" + "בתאריך: " + "<span class='modal-details'>" + day +"/"+ month +"/"+ year+ "</span><br> לתספורת: " + "<span class='modal-details'>" + type +"</span>" );
	$("#myModal").modal();

	
	return false;

});










/*****************************************************************************************/
/**********************************FUNCTIONS**********************************************/
/*****************************************************************************************/
function openTimePicker(currentDate){
	$(".hoursWrapper").hide();
	var checkFriday = $('#datepicker').datepicker('getDate').getDay() + 1;

	$( ".hoursWrapper .hour" ).each(function( index ) {
		$( this ).show();
	});
	if (checkFriday == 6){
		$( ".removeOnFriday" ).hide();
	}

	day = currentDate[0];
	month = currentDate[1];
	year = currentDate[2];


	$.post( "db.php", { getCustomers: "getCustomers" ,day: day ,month: month ,year: year })
	.done(function(  data ) {
		var data = $.parseJSON( data );
		console.log(data);
		for(var date in data){
			var hourFromDb = data[date]['hour'];
			if (data[date]['hour'].charAt(0) == "0")
				hourFromDb = data[date]['hour'].charAt(1);
			$( ".hoursWrapper .hour:contains('"+ hourFromDb+"')").hide();
		}
		checkType();
		$(".hoursWrapper").fadeIn();

	});
}

function checkType(){
	if ($(".cs-select span").html() == "פן"){
		$(".error").html("אנא בחר שעה שלמה");
		hoverTwice = true;
		$( ".hour" ).each(function( index ) {
			if ( !($(this).css('display') == 'block' && $(this).next().css('display') == 'block')){
				$(this).hide();
			}
		});
		$( ".hour" ).hover(
				function() {
					$( this ).addClass( "hourHover");
					$( this ).next().addClass( "hourHover");
				}, function() {
					$( this ).removeClass( "hourHover" );
					$( this ).next().removeClass( "hourHover");
				}
		);
	}
	else{
		hoverTwice = false;
		$( ".hour" ).hover(
				function() {
					$( this ).addClass( "hourHover");
					$( this ).next().removeClass( "hourHover");
				}, function() {
					$( this ).removeClass( "hourHover" );
					$( this ).next().removeClass( "hourHover");
				}
		);
	}
}


/**********************************MAP**********************************************/
var pos;
function initMap() {
	var directionsDisplay = new google.maps.DirectionsRenderer;
	var directionsService = new google.maps.DirectionsService;
	var map = new google.maps.Map(document
			.getElementById('map'), {
		zoom : 14,
		center : {
			lat : 31.894697,
			lng : 34.793264
		}
	});
	directionsDisplay.setMap(map);

	var infoWindow = new google.maps.InfoWindow({
		map : map
	});

	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(
				position) {
			pos = {
					lat : position.coords.latitude,
					lng : position.coords.longitude
			};

			infoWindow.setPosition(pos);
			infoWindow.setContent('צא/י לדרך');
			map.setCenter(pos);
			calculateAndDisplayRoute(directionsService,
					directionsDisplay);
		}, function() {
			handleLocationError(true, infoWindow, map
					.getCenter());
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map
				.getCenter());
	}

}

function calculateAndDisplayRoute(directionsService,
		directionsDisplay) {
	var selectedMode = 'DRIVING';
	directionsService
	.route(
			{
				origin : {
					lat : pos.lat,
					lng : pos.lng
				}, // Haight.
				destination : {
					lat : 31.894697,
					lng : 34.793264
				}, // Ocean Beach.
				// Note that Javascript allows us to access the constant
				// using square brackets and a string value as its
				// "property."
				travelMode : google.maps.TravelMode[selectedMode]
			},
			function(response, status) {
				if (status == 'OK') {
					directionsDisplay
					.setDirections(response);
				} else {
					window
					.alert('Directions request failed due to '
							+ status);
				}
			});
}

function handleLocationError(browserHasGeolocation,
		infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow
	.setContent(browserHasGeolocation ? 'אנחנו כאן.'
			: 'Error: Your browser doesn\'t support geolocation.');
}		

(function() {
	[].slice.call(document.querySelectorAll('select.cs-select'))
	.forEach(function(el) {
		new SelectFx(el);
	});
})();

!function(a) {
	"use strict";
	a("a.page-scroll").bind("click", function(e) {
		var l=a(this);
		a("html, body").stop().animate( {
			scrollTop: a(l.attr("href")).offset().top-50
		}
		,
		1250,
		"easeInOutExpo"),
		e.preventDefault()
	}
	),
	a("body").scrollspy( {
		target: ".navbar-fixed-top", offset: 51
	}
	),
	a(".navbar-collapse ul li a").click(function() {
		a(".navbar-toggle:visible").click()
	}
	),
	a("#mainNav").affix( {
		offset: {
			top: 100
		}
	}
	)
}
(jQuery);
