<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("analyticstracking.php")?>
<meta charset="utf-8">
<link rel="shortcut icon" href="content/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="מעצב שיער - אבי לוי">
<meta name="author" content="Roni Chabra">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

<title>אבי לוי - עיצוב שיער</title>

<link rel="stylesheet" href="content/css/animated.css">
<link rel="stylesheet" href="content/css/cs-select.css" />
<link rel="stylesheet" href="content/css/magnific-popup.css">
<link rel="stylesheet" href="content/css/creative.min.css">
<link rel="stylesheet" href="content/css/jquery-ui.css">
<link rel="stylesheet" href="content/css/bootstrap.min.css">
<link rel="stylesheet" href="content/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="content/css/style.css">

<script src="content/js/jquery.min.js"></script>
<script src="content/js/jquery.cookie.js"></script>
<script src="content/js/md5.min.js"></script>
<script src="content/js/bootstrap.min.js"></script>
<script src="content/js/scrollreveal.min.js"></script>
<script src="content/js/jquery.magnific-popup.min.js"></script>
<script src="content/js/selectFx.js"></script>
<script src="content/js/jquery-ui.js"></script>
<script src="content/js/datepicker-he.js"></script>
</head>

<body id="page-top">
	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7ZZR7L"
			height="0" width="0" style="display: none; visibility: hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->

	<audio>
		<source src="content/img/scissor.mp3"></source>
	</audio>

	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#openMenu">
					<i class="fa fa-bars"></i> תפריט
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"
					style='width: 175px; height: 53px;'><img src="content/img/logo.png"></a>

				<button class="btn btn-primary loginButton web" data-toggle="modal"
					data-target="#loginModal" >התחבר</button>
			</div>
			<div class="collapse navbar-collapse" id="openMenu">
				<ul class="nav navbar-nav navbar-right cl-effect-13">
					<li><a class="page-scroll" href="#top">דף הבית</a></li>
					<li><a class="page-scroll" href="#schedule">קביעת תור</a></li>
					<li><a class="page-scroll" href="#gallery">גלרייה</a></li>
					<li><a class="page-scroll" href="#contact">אודות</a></li>
					<li><a class='loginButton mobile' data-toggle="modal"
					data-target="#loginModal"  style='background-color: transparent;    cursor: pointer; border: 0px;'>התחבר</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<header>
		<section id="top"></section>

		<div class="header-content">
			<div class="header-content-inner">
				<h1 id="scheduleHeading">אבי לוי - עיצוב שיער</h1>
				<hr>
				<p>שלום לכולם, זהו האתר שלי</p>
				<!-- 
				<a href="#schedule"
					class="btn btn-info btn-xl page-scroll schedulaeBTScroll cl-effect-2">לחצו
					להזמנת תור </a>
					 -->
					
				<nav class="cl-effect-8" id="cl-effect-8">
					<a href="#schedule" class='page-scroll schedulaeBTScroll'>להזמנת תור</a>
				</nav>
					
					
					<a href="#schedule"
					class="page-scroll animated infinite bounce"><img
					src='content/img/menu_down_arrow.png' class="godwon"></a>
					
			</div>
		</div>
	</header>

	<section class="bg-primary" id="schedule">
		<div class="container">
			<div class="row">
				<img src="content/img/‏‏arrow-up-white.png"  id='arrowUpToClendar'/>
				<div class="col-lg-8 col-lg-offset-2 text-center details">
					<h2 class="section-heading">קביעת תור</h2>
					<p class="text-faded">הזינו פרטים אישיים</p>
					<form method="post">
						<!--https://codepen.io/sevilayha/pen/IdGKH-->
						<div class="container">
							<div class="row">
								<div class="col-sm-4 get_form_inner">
									<div class="group">
										<input type="text" name="FirstName" required="required"
											id='name'> <span class="highlight"></span> <span class="bar"></span><label>*שם
											מלא</label>
									</div>

									<div class="group">
										<input type="number" name="Phone" required="required"
											id='phone' maxlength='10'> <span class="highlight"></span> <span
											class="bar"></span><label>*טלפון</label>
									</div>
									<div class="group">
										<input type="text" name="info" required="required" id='info'>
										<span class="highlight"></span> <span class="bar"></span><label>הערות</label>
									</div>


									<div>
										<select class="cs-select cs-skin-underline" id="hairCutOption"
											required="required">
											<option value="0" disabled selected>בחר\י תספורת</option>
											<option value="1">החלקה</option>
											<option value="2">קצוץ</option>
											<option value="3">פן</option>
											<option value="4">קארה</option>
											<option value="5">צבע</option>
										</select>
									</div>
								</div>
							</div>
						</div>
				
				</div><!-- 
				<img src="content/img/arrow-down-white.png"  id='arrowDownToClendar'/> -->
				<div class="error"></div>

				<div id="datepicker"></div>
				<div class='hoursWrapper'>
					<div class="hour" data-id="1">9:00</div>
					<div class="hour" data-id="2">9:30</div>
					<div class="hour" data-id="3">10:00</div>
					<div class="hour" data-id="4">10:30</div>
					<div class="hour" data-id="5">11:00</div>
					<div class="hour" data-id="6">11:30</div>
					<div class="hour" data-id="7">12:00</div>
					<div class="hour" data-id="8">12:30</div>
					<div class="hour" data-id="9">13:00</div>
					<div class="hour removeOnFriday" data-id="10">13:30</div>
					<div class="hour removeOnFriday" data-id="11">14:00</div>
					<div class="hour removeOnFriday " data-id="12">14:30</div>
					<div class="hour removeOnFriday" data-id="13">15:00</div>
					<div class="hour removeOnFriday" data-id="14">15:30</div>
					<div class="hour removeOnFriday" data-id="15">16:00</div>
					<div class="hour removeOnFriday" data-id="16">16:30</div>
					<div class="hour removeOnFriday" data-id="17">17:00</div>
					<div class="hour removeOnFriday" data-id="18">17:30</div>
					<div class="hour removeOnFriday" data-id="19">18:00</div>
					<div class="hour removeOnFriday" data-id="20">18:30</div>
					<div class="hour removeOnFriday" data-id="21">19:00</div>
					<div class="hour removeOnFriday" data-id="22">19:30</div>
				</div>
				<img src='content/img/ring.gif' id='ring' />

				<div class="color-10 newOrder">
					<nav class="cl-effect-10">
						<a href="#cl-effect-10" data-hover="אישור הזמנה" class='submitNewOrder' ><span>אישור הזמנה</span></a>
					</nav>
				</div>
				
				<div class="color-10" id='arrowDownToClendar'>
					<nav class="cl-effect-10">
						<a href="#cl-effect-10" data-hover="המשך"><span>המשך</span></a>
					</nav>
				</div>
			
					
				</form>

				<!-- Modal -->
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">אישור הזמנה</h4>
							</div>
							<div class="modal-body">
								<p></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">ביטול</button>
								<button type="button" class="btn btn-default"
									data-dismiss="modal" id="acceptSchedule">אישור</button>
							</div>
						</div>

					</div>
				</div>
				<!-- END MODAL -->

				<!-- Modal -->
				<div class="modal fade" id="reminderModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">תזכורת</h4>
							</div>
							<div class="modal-body">
								<p></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">רשמתי!</button>
							</div>
						</div>

					</div>
				</div>
				<!-- END MODAL -->
				
			</div>
		</div>
		</div>
	</section>

	<section class="no-padding" id='gallery'>
		<div class="container-fluid">
			<div class="row no-gutter popup-gallery">
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/1big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/1.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-החלקה text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>


				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/3big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/3.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/4big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/4.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/5big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/5.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/8big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/8.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/6big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/6.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/7big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/7.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/2big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/2.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-sm-6">
					<a href="content/img/haircuts/9big.jpg" class="portfolio-box"> <img
						src="content/img/haircuts/9.jpg" class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">תספורת</div>
								<div class="project-name">החלקה</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>



	<section id="contact">
		<div class="container">
			<div class="row">
				<h2>אודות</h2>
				<p>מספרת אבי לוי הוקמה בשנת 1990, שאהבת האדם והתשוקה לעיצוב שיער
					הובילה להגשמת החלום: לפתוח מספרה בדמותו. סימן ההיכר של המספרה הוא
					היחס החם והשירות מקצועי, שגורמים לכל מי שנכנס למספרה לצאת ממנה עם
					שמחה בלב. המומחיות של אבי לוי מתבססת על ניסיון רב שנים, שימוש
					בחומרים הכי איכותיים והכי חשוב – היכולת להבין איזה לוק יגרום לך
					להרגיש הכי יפה בעולם..</p>

				<div class="container contactWrapper">
				<div id="map"></div>
					<div class="row contact">
					<article id='waze'>
					
							<a href="waze://?ll= 31.894697,34.793264" target="_blank"
								class="cmn-t-border col-lg-4 col-md-6 text-center"> <span class="glyphicon  glyphicon-map-marker" aria-hidden="true"></span>
								
								<h3>פתח כתובת ב Waze</h3>
							</a>
						</article>
						<article>
							<a href="https://www.facebook.com" target="_blank"
								class="cmn-t-border col-lg-4 col-md-6 text-center"><img id='facebookIcon' src='content/img/facebook.png'/>
								<h3>Avi levi </h3>
							</a>
						</article>
						<article>
							<a href="mailto:Avi.levi@gmail.com"
								class="cmn-t-border col-lg-4 col-md-6 text-center"> <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
								<h3>Avi.levi@gmail.com </h3>
							</a>
						</article>
						<article>
							<a href="tel:08-9456789"
								class="cmn-t-border col-lg-4 col-md-6 text-center"> <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
								<h3>
								050-2560000 </h3>
							</a>
						</article>
					</div>
				</div>
			</div>
	
	</section>

	<!-- LOGIN MODAL -->
	<div class="modal fade" id="loginModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">מסך כניסה</h4>
				</div>
				<!-- /.modal-header -->

				<div class="modal-body">
					<form role="form">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control" id="uLogin"
									placeholder="Login"> <label for="uLogin"
									class="input-group-addon glyphicon glyphicon-user"></label>
							</div>
						</div>
						<!-- /.form-group -->

						<div class="form-group">
							<div class="input-group">
								<input type="password" class="form-control" id="uPassword"
									placeholder="Password"> <label for="uPassword"
									class="input-group-addon glyphicon glyphicon-lock"></label>
							</div>
							<!-- /.input-group -->
						</div>
						<!-- /.form-group -->

						<div class="checkbox">
							<label> <input type="checkbox" id='rememberMe'/>
								<p>זכור אותי</p>
							</label>
						</div>
						<!-- /.checkbox -->
					</form>

				</div>
				<!-- /.modal-body -->

				<div class="modal-footer">
					<button class="form-control btn btn-primary loginManager">אישור</button>

					<div class="progress">
						<div class="progress-bar progress-bar-primary" role="progressbar"
							aria-valuenow="1" aria-valuemin="1" aria-valuemax="100"
							style="width: 0%;">
							<span class="sr-only">בתהליך...</span>
						</div>
					</div>
				</div>
				<!-- /.modal-footer -->

			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- END LOGIN MODAL -->
	
	

<!-- keep creative at the bottom (gallery) -->
<script src="content/js/creative.min.js"></script>
<script src="content/js/script.js"></script>
</body>
</html>
