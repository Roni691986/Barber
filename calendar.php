<html>
<head>
<?php include_once("analyticstracking.php")?>
<meta charset="utf-8">
<link rel="shortcut icon" href="content/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="מעצב שיער - אבי לוי">
<meta name="author" content="Roni Chabra">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

<title>אבי לוי - עיצוב שיער</title>
<link rel="stylesheet" href="content/css/calendar.css" />
<link rel="stylesheet" href="content/css/custom_1.css" />
<link rel="stylesheet" href="content/css/bootstrap.min.css">
<link rel="stylesheet" href="content/css/cs-select.css" />
<link rel="stylesheet" href="content/css/calendar_style.css">
<link rel="stylesheet" href="content/css/style.css">

<script src="content/js/jquery.min.js"></script>
<script src="content/js/calendar.js"></script>
<script src="content/js/modernizr.custom.63321.js"></script>
<script src="content/js/bootstrap.min.js"></script>
<script src="content/js/classie.js"></script>
<script src="content/js/selectFx.js"></script>
<script src="content/js/jquery.calendario.js"></script>
</head>

<body>
<a href='index.php' class="btn btn-primary" id='homePage'>עמוד הבית</a>
	<div class="pager">
		
		<button class="pagerBT currentViewTab" id="monthView">
			<span>תצוגת חודש</span>
		</button>
		
		<button class="pagerBT" id="weekView">
			<span>תצוגת שבוע</span>
		</button>
		
		<button class="pagerBT" id="dayView">
			<span>תצוגת יום</span>
		</button>

	</div>
	<ul class="week">
		<li>יום א</li>
		<li>יום ב</li>
		<li>יום ג</li>
		<li>יום ד</li>
		<li>יום ה</li>
		<li>יום ו</li>
	</ul>
	<div class="dayTable">
	<table class="table table-striped custab">
	</table>
	</div>
	<div class="weekTable"></div>
	<div class="monthTable">
		<div class="custom-calendar-wrap custom-calendar-full">
			<div class="custom-header clearfix">
				
				<h3 class="custom-month-year">
				<nav>
						<span id="custom-next" class="custom-prev">חודש הבא</span> <span
							id="custom-current" class="custom-current"
							title="Got to current date"></span> <span
							id="custom-prev" class="custom-next">חודש קודם</span>
					</nav>
					
					<span id="custom-month" class="custom-month"></span> <span
						id="custom-year" class="custom-year"></span>
				
				</h3>
			</div>
			<div id="calendar" class="fc-calendar-container">
				<div class="fc-calendar fc-five-rows">
					<div class="fc-head">
						<div>Monday</div>
						<div>Tuesday</div>
						<div>Wednesday</div>
						<div>Thursday</div>
						<div>Friday</div>
						<div>Saturday</div>
						<div>Sunday</div>
					</div>
					<div class="fc-body">
						<div class="fc-row">
							<div></div>
							<div></div>
							<div>
								<span class="fc-date">1</span><span class="fc-weekday">Wed</span>
							</div>
							<div>
								<span class="fc-date">2</span><span class="fc-weekday">Thu</span>
							</div>
							<div>
								<span class="fc-date">3</span><span class="fc-weekday">Fri</span>
							</div>
							<div>
								<span class="fc-date">4</span><span class="fc-weekday">Sat</span>
							</div>
							<div>
								<span class="fc-date">5</span><span class="fc-weekday">Sun</span>
							</div>
						</div>
						<div class="fc-row">
							<div>
								<span class="fc-date">6</span><span class="fc-weekday">Mon</span>
							</div>
							<div>
								<span class="fc-date">7</span><span class="fc-weekday">Tue</span>
							</div>
							<div class="fc-today ">
								<span class="fc-date">8</span><span class="fc-weekday">Wed</span>
							</div>
							<div>
								<span class="fc-date">9</span><span class="fc-weekday">Thu</span>
							</div>
							<div>
								<span class="fc-date">10</span><span class="fc-weekday">Fri</span>
							</div>
							<div>
								<span class="fc-date">11</span><span class="fc-weekday">Sat</span>
							</div>
							<div>
								<span class="fc-date">12</span><span class="fc-weekday">Sun</span>
							</div>
						</div>
						<div class="fc-row">
							<div>
								<span class="fc-date">13</span><span class="fc-weekday">Mon</span>
							</div>
							<div>
								<span class="fc-date">14</span><span class="fc-weekday">Tue</span>
							</div>
							<div>
								<span class="fc-date">15</span><span class="fc-weekday">Wed</span>
							</div>
							<div>
								<span class="fc-date">16</span><span class="fc-weekday">Thu</span>
							</div>
							<div>
								<span class="fc-date">17</span><span class="fc-weekday">Fri</span>
							</div>
							<div>
								<span class="fc-date">18</span><span class="fc-weekday">Sat</span>
							</div>
							<div>
								<span class="fc-date">19</span><span class="fc-weekday">Sun</span>
							</div>
						</div>
						<div class="fc-row">
							<div>
								<span class="fc-date">20</span><span class="fc-weekday">Mon</span>
							</div>
							<div>
								<span class="fc-date">21</span><span class="fc-weekday">Tue</span>
							</div>
							<div>
								<span class="fc-date">22</span><span class="fc-weekday">Wed</span>
							</div>
							<div>
								<span class="fc-date">23</span><span class="fc-weekday">Thu</span>
							</div>
							<div>
								<span class="fc-date">24</span><span class="fc-weekday">Fri</span>
							</div>
							<div>
								<span class="fc-date">25</span><span class="fc-weekday">Sat</span>
							</div>
							<div>
								<span class="fc-date">26</span><span class="fc-weekday">Sun</span>
							</div>
						</div>
						<div class="fc-row">
							<div>
								<span class="fc-date">27</span><span class="fc-weekday">Mon</span>
							</div>
							<div>
								<span class="fc-date">28</span><span class="fc-weekday">Tue</span>
							</div>
							<div>
								<span class="fc-date">29</span><span class="fc-weekday">Wed</span>
							</div>
							<div>
								<span class="fc-date">30</span><span class="fc-weekday">Thu</span>
							</div>
							<div>
								<span class="fc-date">31</span><span class="fc-weekday">Fri</span>
							</div>
							<div></div>
							<div></div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>

	<div id="particles-js"></div>

	<!-- Modal -->
	<div class="modal fade" id="barberModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">ביטול הזמנה</h4>
				</div>
				<div class="modal-body">
					<p></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">ביטול</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						id="acceptRemoveSchedule">אישור</button>
				</div>
			</div>

		</div>
	</div>
	<!-- END MODAL -->
	
	<!-- ORDER Modal -->
	<div class="modal fade" id="orderModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">הזמנה</h4>
				</div>
				<div class="modal-body">
					<div class="group">
										<input type="text" name="FirstName" required="required"
											id='name'> <span class="highlight"></span> <span
											class="bar"></span><label>*שם מלא</label>
									</div>

									<div class="group">
										<input type="number" name="Phone" required="required"
											id='phone' maxlength='10'> <span class="highlight"></span>
										<span class="bar"></span><label>*טלפון</label>
									</div>
									<div class="group">
										<input type="text" name="info" required="required" id='info'>
										<span class="highlight"></span> <span class="bar"></span><label>הערות</label>
									</div>
									<div class="form-group">
									  <label for="sel1" style='float: right;'>בחר שעה</label>
									  <select class="form-control" id="selectHour">
									    <option selected="selected" value="09:00">9:00</option>
									    <option value="09:30">9:30</option>
									    <option value="10:00">10:00</option>
									    <option value="10:30">10:30</option>
									     <option value="11:00">11:00</option>
									    <option value="11:30">11:30</option>
									     <option value="12:00">12:00</option>
									    <option value="12:30">12:30</option>
									     <option value="13:00">13:00</option>
									    <option value="13:30">13:30</option>
									     <option value="14:00">14:00</option>
									    <option value="14:30">14:30</option>
									     <option value="15:00">15:00</option>
									    <option value="15:30">15:30</option>
									     <option value="16:00">16:00</option>
									    <option value="16:30">16:30</option>
									     <option value="17:00">17:00</option>
									    <option value="17:30">17:30</option>
									  </select>
									</div>

									<div>
										<select class="form-control" id="hairCutOption"
											required="required">
											<option value="0" disabled selected>בחר\י תספורת</option>
											<option value="1">החלקה</option>
											<option value="2">קצוץ</option>
											<option value="3">פן</option>
											<option value="4">קארה</option>
											<option value="5">צבע</option>
										</select>
									</div>
				</div>
				<div class="modal-footer">
				<p class="error"></p>
					<button type="button" class="btn btn-default" data-dismiss="modal">ביטול</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						id="acceptSchedule">אישור</button>
				</div>
			</div>

		</div>
	</div>
	<!-- END MODAL -->


</body>
</html>